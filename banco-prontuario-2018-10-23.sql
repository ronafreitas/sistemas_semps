-- MySQL dump 10.13  Distrib 5.7.23, for Linux (x86_64)
--
-- Host: localhost    Database: prontuario
-- ------------------------------------------------------
-- Server version	5.7.23-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `anotacao_acolhimento_familiar`
--

DROP TABLE IF EXISTS `anotacao_acolhimento_familiar`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `anotacao_acolhimento_familiar` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_atendimento` int(10) unsigned NOT NULL,
  `perda_domicilio` tinyint(1) DEFAULT '0' COMMENT 'por perda temporária de domicílio\\n',
  `crianca_guarda` tinyint(1) DEFAULT '0' COMMENT 'criança sob guarda de outro\n',
  `adulto_prisional` tinyint(1) DEFAULT '0' COMMENT 'adulto em situação prisional\n',
  `em_internacao` tinyint(1) DEFAULT '0',
  `periodo_perda_inicio` datetime DEFAULT NULL,
  `periodo_perda_fim` datetime DEFAULT NULL,
  `motivo_perda` text CHARACTER SET latin1 COMMENT 'motivo da perda temporaria',
  `periodo_guarda_inicio` datetime DEFAULT NULL COMMENT 'periodo da guarda - data de inicio',
  `periodo_guarda_fim` datetime DEFAULT NULL COMMENT 'periodo da guarda - data fim',
  `razao_guarda` text CHARACTER SET latin1 COMMENT 'razão da guarda por outro',
  `detentor_guarda` varchar(255) CHARACTER SET latin1 DEFAULT NULL COMMENT 'detentor da guarda',
  `observacoes` text CHARACTER SET latin1,
  `atualizado_em` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `criado_em` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `id_atendimento_UNIQUE` (`id_atendimento`),
  CONSTRAINT `fk_acolhimento_1` FOREIGN KEY (`id_atendimento`) REFERENCES `atendimento` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `anotacao_acolhimento_familiar`
--

LOCK TABLES `anotacao_acolhimento_familiar` WRITE;
/*!40000 ALTER TABLE `anotacao_acolhimento_familiar` DISABLE KEYS */;
/*!40000 ALTER TABLE `anotacao_acolhimento_familiar` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `anotacao_acolhimento_familiar_membro`
--

DROP TABLE IF EXISTS `anotacao_acolhimento_familiar_membro`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `anotacao_acolhimento_familiar_membro` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_anotacao_acolhimento_familiar` int(10) unsigned NOT NULL,
  `id_membro` varchar(45) DEFAULT NULL,
  `inicio` datetime DEFAULT NULL,
  `fim` datetime DEFAULT NULL,
  `motivacao` varchar(200) DEFAULT NULL,
  `instituicao_acolhimento` varchar(150) DEFAULT NULL,
  `responsavel_guarda` varchar(80) DEFAULT NULL,
  `atualizado_em` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `criado_em` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `id_acolhimento_familiar_UNIQUE` (`id_anotacao_acolhimento_familiar`),
  CONSTRAINT `fk_acolhimento_familiar_membros_1` FOREIGN KEY (`id_anotacao_acolhimento_familiar`) REFERENCES `anotacao_acolhimento_familiar` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `anotacao_acolhimento_familiar_membro`
--

LOCK TABLES `anotacao_acolhimento_familiar_membro` WRITE;
/*!40000 ALTER TABLE `anotacao_acolhimento_familiar_membro` DISABLE KEYS */;
/*!40000 ALTER TABLE `anotacao_acolhimento_familiar_membro` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `anotacao_acompanhamento_familiar`
--

DROP TABLE IF EXISTS `anotacao_acompanhamento_familiar`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `anotacao_acompanhamento_familiar` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_atendimento` int(10) unsigned NOT NULL,
  `inclusao` datetime DEFAULT NULL,
  `desligamento` datetime DEFAULT NULL,
  `razao_desligamento` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `planejamento_inicial` text CHARACTER SET latin1 COMMENT 'planejamento inicial',
  `evolucao_acompanhamento` text CHARACTER SET latin1 COMMENT 'evolução do acompanhamento familiar',
  `atualizado_em` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `criado_em` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `id_atendimento_UNIQUE` (`id_atendimento`),
  CONSTRAINT `fk_acompanhamento_familiar_1` FOREIGN KEY (`id_atendimento`) REFERENCES `atendimento` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `anotacao_acompanhamento_familiar`
--

LOCK TABLES `anotacao_acompanhamento_familiar` WRITE;
/*!40000 ALTER TABLE `anotacao_acompanhamento_familiar` DISABLE KEYS */;
/*!40000 ALTER TABLE `anotacao_acompanhamento_familiar` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `anotacao_beneficio`
--

DROP TABLE IF EXISTS `anotacao_beneficio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `anotacao_beneficio` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_atendimento` int(10) unsigned NOT NULL,
  `id_tipo_beneficio` int(10) unsigned NOT NULL,
  `data` datetime DEFAULT NULL,
  `tipo_documento` varchar(200) CHARACTER SET latin1 DEFAULT NULL,
  `registro` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `observacoes` text CHARACTER SET latin1,
  `atualizado_em` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `criado_em` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `id_tipo_beneficio_UNIQUE` (`id_tipo_beneficio`),
  UNIQUE KEY `id_atendimento_UNIQUE` (`id_atendimento`),
  CONSTRAINT `fk_beneficio_1` FOREIGN KEY (`id_atendimento`) REFERENCES `atendimento` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_beneficios_1` FOREIGN KEY (`id_tipo_beneficio`) REFERENCES `tipo_beneficio` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `anotacao_beneficio`
--

LOCK TABLES `anotacao_beneficio` WRITE;
/*!40000 ALTER TABLE `anotacao_beneficio` DISABLE KEYS */;
/*!40000 ALTER TABLE `anotacao_beneficio` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `anotacao_composicao_familiar`
--

DROP TABLE IF EXISTS `anotacao_composicao_familiar`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `anotacao_composicao_familiar` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_atendimento` int(10) unsigned NOT NULL,
  `situacao_rua` tinyint(1) DEFAULT '0' COMMENT 'em situação de rua',
  `familia_quilombola` tinyint(1) DEFAULT '0',
  `familia_ribeirinha` tinyint(1) DEFAULT '0',
  `familia_cigana` tinyint(1) DEFAULT '0',
  `familia_indigena_1` tinyint(1) DEFAULT '0' COMMENT 'família indígena residente em aldeia/reserva',
  `familia_indigena_2` tinyint(1) DEFAULT '0',
  `etnia_1` varchar(150) CHARACTER SET latin1 DEFAULT NULL,
  `etnia_2` varchar(150) DEFAULT NULL,
  `observacoes` text CHARACTER SET latin1,
  `atualizado_em` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `criado_em` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `id_atendimento_UNIQUE` (`id_atendimento`),
  CONSTRAINT `fk_composicao_familiar_1` FOREIGN KEY (`id_atendimento`) REFERENCES `atendimento` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `anotacao_composicao_familiar`
--

LOCK TABLES `anotacao_composicao_familiar` WRITE;
/*!40000 ALTER TABLE `anotacao_composicao_familiar` DISABLE KEYS */;
/*!40000 ALTER TABLE `anotacao_composicao_familiar` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `anotacao_composicao_familiar_membro`
--

DROP TABLE IF EXISTS `anotacao_composicao_familiar_membro`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `anotacao_composicao_familiar_membro` (
  `id` int(10) unsigned NOT NULL,
  `id_anotacao_composicao_familiar` int(10) unsigned NOT NULL,
  `nome` varchar(255) CHARACTER SET latin1 NOT NULL COMMENT 'nome da pessoa de referência',
  `sexo` enum('Masculino','Feminino') CHARACTER SET latin1 DEFAULT NULL,
  `nascimento` datetime DEFAULT NULL,
  `possui_deficiencia` tinyint(1) DEFAULT '0',
  `parentesco` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `cn` tinyint(1) DEFAULT '0',
  `rg` tinyint(1) DEFAULT '0',
  `ctps` tinyint(1) DEFAULT '0',
  `cpf` tinyint(1) DEFAULT '0',
  `te` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `id_composicao_familiar_UNIQUE` (`id_anotacao_composicao_familiar`),
  CONSTRAINT `fk_composicao_familiar_membros_1` FOREIGN KEY (`id_anotacao_composicao_familiar`) REFERENCES `anotacao_composicao_familiar` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `anotacao_composicao_familiar_membro`
--

LOCK TABLES `anotacao_composicao_familiar_membro` WRITE;
/*!40000 ALTER TABLE `anotacao_composicao_familiar_membro` DISABLE KEYS */;
/*!40000 ALTER TABLE `anotacao_composicao_familiar_membro` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `anotacao_condicoes_habitacionais`
--

DROP TABLE IF EXISTS `anotacao_condicoes_habitacionais`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `anotacao_condicoes_habitacionais` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_atendimento` int(10) unsigned NOT NULL,
  `tipo_residencia` enum('Abc','XYZ') CHARACTER SET latin1 DEFAULT NULL,
  `material_paredes` enum('Abc','XYZ') CHARACTER SET latin1 DEFAULT NULL,
  `acesso_energia` enum('Abc','XYZ') CHARACTER SET latin1 DEFAULT NULL,
  `agua_canalisada` enum('Abc','XYZ') CHARACTER SET latin1 DEFAULT NULL,
  `forma_abastecimento` enum('Abc','XYZ') CHARACTER SET latin1 DEFAULT NULL,
  `escoamento_sanitario` enum('Abc','XYZ') CHARACTER SET latin1 DEFAULT NULL,
  `coleta_lixo` enum('Abc','XYZ') CHARACTER SET latin1 DEFAULT NULL,
  `numero_comodos` char(2) CHARACTER SET latin1 DEFAULT NULL,
  `numero_dormitorios` char(2) CHARACTER SET latin1 DEFAULT NULL,
  `pessoas_por_dormitorio` char(2) CHARACTER SET latin1 DEFAULT NULL,
  `caracteristicas_risco` tinyint(1) DEFAULT NULL,
  `caracteristicas_dificil_acesso` tinyint(1) DEFAULT NULL,
  `caracteristicas_conflito` tinyint(1) DEFAULT NULL,
  `observacoes` text CHARACTER SET latin1,
  `atualizado_em` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `criado_em` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `id_atendimento_UNIQUE` (`id_atendimento`),
  CONSTRAINT `fk_condicoes_habitacionais_1` FOREIGN KEY (`id_atendimento`) REFERENCES `atendimento` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `anotacao_condicoes_habitacionais`
--

LOCK TABLES `anotacao_condicoes_habitacionais` WRITE;
/*!40000 ALTER TABLE `anotacao_condicoes_habitacionais` DISABLE KEYS */;
/*!40000 ALTER TABLE `anotacao_condicoes_habitacionais` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `anotacao_convivencia_familiar`
--

DROP TABLE IF EXISTS `anotacao_convivencia_familiar`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `anotacao_convivencia_familiar` (
  `id` int(10) unsigned NOT NULL,
  `id_atendimento` int(10) unsigned NOT NULL,
  `pessoas_sem_adultos` tinyint(1) DEFAULT NULL COMMENT 'pessoas ficam sem adultos durante o dia',
  `vitima_ameaca_discriminacao` tinyint(1) DEFAULT NULL COMMENT 'é vítima de ameaça ou discriminação na comunidade',
  `sempre_morou_no_estado` tinyint(1) DEFAULT NULL,
  `tempo_no_estado` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `sempre_morou_no_municipio` tinyint(1) DEFAULT NULL,
  `tempo_no_municipio` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `sempre_morou_no_bairro` tinyint(1) DEFAULT NULL,
  `tempo_no_bairro` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `participa_de_grupos` tinyint(1) DEFAULT NULL COMMENT 'participa de grupos que são rede de apoio',
  `crianca_sem_lazer` tinyint(1) DEFAULT '0' COMMENT 'Criança ou adolescente sem atividade de lazer, recreação ou convívio',
  `idoso_sem_lazer` tinyint(1) DEFAULT NULL COMMENT 'idoso sem atividade de lazer, recreação ou convívio',
  `relacoes_conjugais` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `relacoes_conflito_outros` varchar(255) CHARACTER SET latin1 DEFAULT NULL COMMENT 'relações conflituosas com outros',
  `observacoes` text CHARACTER SET latin1,
  `atualizado_em` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `criado_em` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `id_atendimento_UNIQUE` (`id_atendimento`),
  CONSTRAINT `fk_convivencia_familiar_1` FOREIGN KEY (`id_atendimento`) REFERENCES `atendimento` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `anotacao_convivencia_familiar`
--

LOCK TABLES `anotacao_convivencia_familiar` WRITE;
/*!40000 ALTER TABLE `anotacao_convivencia_familiar` DISABLE KEYS */;
/*!40000 ALTER TABLE `anotacao_convivencia_familiar` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `anotacao_educacional`
--

DROP TABLE IF EXISTS `anotacao_educacional`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `anotacao_educacional` (
  `id` int(10) unsigned NOT NULL,
  `id_atendimento` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ocorrencia` datetime DEFAULT NULL,
  `efeito` varchar(150) CHARACTER SET latin1 DEFAULT NULL,
  `observacoes` text CHARACTER SET latin1,
  `atualizado_em` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `criado_em` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `id_atendimento_UNIQUE` (`id_atendimento`),
  CONSTRAINT `fk_educacional_1` FOREIGN KEY (`id_atendimento`) REFERENCES `atendimento` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `anotacao_educacional`
--

LOCK TABLES `anotacao_educacional` WRITE;
/*!40000 ALTER TABLE `anotacao_educacional` DISABLE KEYS */;
/*!40000 ALTER TABLE `anotacao_educacional` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `anotacao_educacional_familiar_membro`
--

DROP TABLE IF EXISTS `anotacao_educacional_familiar_membro`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `anotacao_educacional_familiar_membro` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_anotacao_educacional` int(10) unsigned NOT NULL,
  `id_anotacao_composicao_familiar_membro` int(10) unsigned NOT NULL,
  `primeiro_nome` varchar(250) DEFAULT NULL,
  `idade` tinyint(2) DEFAULT NULL,
  `le_escreve` tinyint(1) DEFAULT NULL COMMENT 'lê e escreve?',
  `frequenta_escola` tinyint(1) DEFAULT NULL,
  `escolaridade` varchar(45) DEFAULT NULL,
  `nome_unidade_escolar` varchar(200) DEFAULT NULL,
  `rede_publica` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `id_educacional_UNIQUE` (`id_anotacao_educacional`),
  UNIQUE KEY `id_composicao_familiar_membro_UNIQUE` (`id_anotacao_composicao_familiar_membro`),
  CONSTRAINT `fk_anotacao_educacional_familiar_membro_1` FOREIGN KEY (`id_anotacao_composicao_familiar_membro`) REFERENCES `anotacao_composicao_familiar_membro` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_educacional_familia_1` FOREIGN KEY (`id_anotacao_educacional`) REFERENCES `anotacao_educacional` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `anotacao_educacional_familiar_membro`
--

LOCK TABLES `anotacao_educacional_familiar_membro` WRITE;
/*!40000 ALTER TABLE `anotacao_educacional_familiar_membro` DISABLE KEYS */;
/*!40000 ALTER TABLE `anotacao_educacional_familiar_membro` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `anotacao_forma_ingresso_primeiro_atendimento`
--

DROP TABLE IF EXISTS `anotacao_forma_ingresso_primeiro_atendimento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `anotacao_forma_ingresso_primeiro_atendimento` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `opcao` tinytext NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `anotacao_forma_ingresso_primeiro_atendimento`
--

LOCK TABLES `anotacao_forma_ingresso_primeiro_atendimento` WRITE;
/*!40000 ALTER TABLE `anotacao_forma_ingresso_primeiro_atendimento` DISABLE KEYS */;
INSERT INTO `anotacao_forma_ingresso_primeiro_atendimento` VALUES (1,'Em decorrência de encaminhamento realizado por outros serviços/unidades de Proteção Social Básica'),(2,'Outros');
/*!40000 ALTER TABLE `anotacao_forma_ingresso_primeiro_atendimento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `anotacao_fortalecimento`
--

DROP TABLE IF EXISTS `anotacao_fortalecimento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `anotacao_fortalecimento` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_atendimento` int(10) unsigned NOT NULL,
  `observacoes` text CHARACTER SET latin1,
  `atualizado_em` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `criado_em` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `id_atendimento_UNIQUE` (`id_atendimento`),
  CONSTRAINT `fk_fortalecimento_1` FOREIGN KEY (`id_atendimento`) REFERENCES `atendimento` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `anotacao_fortalecimento`
--

LOCK TABLES `anotacao_fortalecimento` WRITE;
/*!40000 ALTER TABLE `anotacao_fortalecimento` DISABLE KEYS */;
/*!40000 ALTER TABLE `anotacao_fortalecimento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `anotacao_fortalecimento_tipo_servico`
--

DROP TABLE IF EXISTS `anotacao_fortalecimento_tipo_servico`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `anotacao_fortalecimento_tipo_servico` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_anotacao_fortalecimento` int(10) unsigned NOT NULL,
  `tipo_servico` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `ingresso` datetime DEFAULT NULL,
  `desligamento` datetime DEFAULT NULL,
  `atualizado_em` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `criado_em` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_fortalecimento_UNIQUE` (`id_anotacao_fortalecimento`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  CONSTRAINT `fk_fortalecimento_tipo_servico_1` FOREIGN KEY (`id_anotacao_fortalecimento`) REFERENCES `anotacao_fortalecimento` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `anotacao_fortalecimento_tipo_servico`
--

LOCK TABLES `anotacao_fortalecimento_tipo_servico` WRITE;
/*!40000 ALTER TABLE `anotacao_fortalecimento_tipo_servico` DISABLE KEYS */;
/*!40000 ALTER TABLE `anotacao_fortalecimento_tipo_servico` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `anotacao_pessoa_referencia`
--

DROP TABLE IF EXISTS `anotacao_pessoa_referencia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `anotacao_pessoa_referencia` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_unidade` int(10) unsigned NOT NULL,
  `id_tecnico` int(10) unsigned NOT NULL,
  `id_anotacao_forma_ingresso_primeiro_atendimento` int(10) unsigned NOT NULL,
  `ultimo_atendimento_data` date NOT NULL,
  `ultimo_atendimento_hora` time NOT NULL,
  `nis` varchar(11) NOT NULL,
  `cpf` varchar(14) NOT NULL,
  `data_prontuario` date NOT NULL,
  `nome` varchar(255) NOT NULL,
  `apelido` varchar(255) DEFAULT NULL,
  `nome_mae` varchar(255) DEFAULT NULL,
  `rg` varchar(10) DEFAULT NULL,
  `orgao_emissor` varchar(45) DEFAULT NULL,
  `uf_documento` varchar(45) DEFAULT NULL,
  `emissao` date DEFAULT NULL,
  `sexo` varchar(45) DEFAULT NULL,
  `nascimento` date DEFAULT NULL,
  `detalhes_outros` varchar(255) DEFAULT NULL,
  `nome_contato_orgao` varchar(255) DEFAULT NULL,
  `motivacao_primeiro_atendimento` text,
  `programa_social_bolsa_familia` tinyint(1) DEFAULT '0',
  `programa_bpc` tinyint(1) DEFAULT '0',
  `programa_peti` tinyint(1) DEFAULT '0',
  `programa_outros` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `id_unidade_UNIQUE` (`id_unidade`),
  KEY `id_tecnico_UNIQUE` (`id_tecnico`),
  KEY `id_anotacao_forma_ingresso_primeiro_atendimento_UNIQUE` (`id_anotacao_forma_ingresso_primeiro_atendimento`),
  CONSTRAINT `fk_anotacao_pessoa_referencia_1` FOREIGN KEY (`id_anotacao_forma_ingresso_primeiro_atendimento`) REFERENCES `anotacao_forma_ingresso_primeiro_atendimento` (`id`),
  CONSTRAINT `fk_anotacao_pessoa_referencia_tecnico` FOREIGN KEY (`id_tecnico`) REFERENCES `tecnico` (`id`),
  CONSTRAINT `fk_anotacao_pessoa_referencia_unidade` FOREIGN KEY (`id_unidade`) REFERENCES `unidade` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `anotacao_pessoa_referencia`
--

LOCK TABLES `anotacao_pessoa_referencia` WRITE;
/*!40000 ALTER TABLE `anotacao_pessoa_referencia` DISABLE KEYS */;
INSERT INTO `anotacao_pessoa_referencia` VALUES (1,14,6,1,'2018-10-10','13:00:00','111','222','2018-10-10','JOSÉ DA SILVA',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,NULL),(3,12,6,1,'2018-10-11','13:00:01','333','444','2018-10-11','MARIA SANTOS',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,NULL);
/*!40000 ALTER TABLE `anotacao_pessoa_referencia` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `anotacao_pessoa_referencia_endereco`
--

DROP TABLE IF EXISTS `anotacao_pessoa_referencia_endereco`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `anotacao_pessoa_referencia_endereco` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_anotacao_pessoa_referencia` int(10) unsigned NOT NULL,
  `cep` varchar(10) DEFAULT NULL,
  `endereco` varchar(255) DEFAULT NULL,
  `numero` tinyint(5) DEFAULT NULL,
  `complemento` varchar(255) DEFAULT NULL,
  `bairro` varchar(255) DEFAULT NULL,
  `municipio` varchar(255) DEFAULT NULL,
  `uf` varchar(45) DEFAULT NULL,
  `ponto_referencia` varchar(255) DEFAULT NULL,
  `telefone` varchar(45) DEFAULT NULL,
  `domicilio` varchar(45) DEFAULT NULL,
  `endereco_abrigo` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `pessoa_id_UNIQUE` (`id_anotacao_pessoa_referencia`),
  KEY `fk_endereco_pessoa1_idx` (`id_anotacao_pessoa_referencia`),
  CONSTRAINT `fk_pessoa_enderecos` FOREIGN KEY (`id_anotacao_pessoa_referencia`) REFERENCES `anotacao_pessoa_referencia` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `anotacao_pessoa_referencia_endereco`
--

LOCK TABLES `anotacao_pessoa_referencia_endereco` WRITE;
/*!40000 ALTER TABLE `anotacao_pessoa_referencia_endereco` DISABLE KEYS */;
/*!40000 ALTER TABLE `anotacao_pessoa_referencia_endereco` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `anotacao_saude`
--

DROP TABLE IF EXISTS `anotacao_saude`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `anotacao_saude` (
  `id` int(10) unsigned NOT NULL,
  `id_atendimento` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `situacao_inseguranca` tinyint(1) DEFAULT '0' COMMENT 'situação de insegurança alimentar',
  `observacoes` text CHARACTER SET latin1,
  `atualizado_em` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `criado_em` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `id_atendimento_UNIQUE` (`id_atendimento`),
  CONSTRAINT `fk_saude_1` FOREIGN KEY (`id_atendimento`) REFERENCES `atendimento` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `anotacao_saude`
--

LOCK TABLES `anotacao_saude` WRITE;
/*!40000 ALTER TABLE `anotacao_saude` DISABLE KEYS */;
/*!40000 ALTER TABLE `anotacao_saude` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `anotacao_saude_familia`
--

DROP TABLE IF EXISTS `anotacao_saude_familia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `anotacao_saude_familia` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_anotacao_saude` int(10) unsigned NOT NULL,
  `id_nome_membro_familia` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `idade` tinyint(2) DEFAULT NULL,
  `tipo_deficiencia` varchar(150) CHARACTER SET latin1 DEFAULT NULL,
  `precisa_cuidador_deficiencia` tinyint(1) DEFAULT '0' COMMENT 'necessita cuidador pela deficiência',
  `responsavel_cuidador_deficiente` varchar(200) CHARACTER SET latin1 DEFAULT NULL COMMENT 'pessoa responsável pelo cuidado',
  `precisa_cuidador_doenca` tinyint(1) DEFAULT '0' COMMENT 'necessita cuidador por idade ou doença',
  `responsavel_cuidador_doenca` varchar(200) CHARACTER SET latin1 DEFAULT NULL,
  `portador_doenca_grave` tinyint(1) DEFAULT '0',
  `usa_remedio_controlado` tinyint(1) DEFAULT '0',
  `usa_alcool` tinyint(1) DEFAULT '0',
  `usa_droga` tinyint(1) DEFAULT '0',
  `meses_gestao` tinyint(2) DEFAULT NULL,
  `inicio_pre_natal` tinyint(1) DEFAULT '0',
  `ocorreu_em` datetime DEFAULT NULL,
  `efeito` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `atualizado_em` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `criado_em` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `id_saude_UNIQUE` (`id_anotacao_saude`),
  CONSTRAINT `fk_saude_familia_1` FOREIGN KEY (`id_anotacao_saude`) REFERENCES `anotacao_saude` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `anotacao_saude_familia`
--

LOCK TABLES `anotacao_saude_familia` WRITE;
/*!40000 ALTER TABLE `anotacao_saude_familia` DISABLE KEYS */;
/*!40000 ALTER TABLE `anotacao_saude_familia` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `anotacao_socioeducativa`
--

DROP TABLE IF EXISTS `anotacao_socioeducativa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `anotacao_socioeducativa` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_atendimento` int(10) unsigned NOT NULL,
  `observacoes` text CHARACTER SET latin1,
  `atualizado_em` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `criado_em` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `id_atendimento_UNIQUE` (`id_atendimento`),
  CONSTRAINT `fk_socioeducativa_1` FOREIGN KEY (`id_atendimento`) REFERENCES `atendimento` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `anotacao_socioeducativa`
--

LOCK TABLES `anotacao_socioeducativa` WRITE;
/*!40000 ALTER TABLE `anotacao_socioeducativa` DISABLE KEYS */;
/*!40000 ALTER TABLE `anotacao_socioeducativa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `anotacao_socioeducativa_familia_membros`
--

DROP TABLE IF EXISTS `anotacao_socioeducativa_familia_membros`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `anotacao_socioeducativa_familia_membros` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_anotacao_socioeducativa` int(10) unsigned NOT NULL,
  `id_sociedade_tipo_medida` int(10) unsigned NOT NULL,
  `inicio` varchar(7) CHARACTER SET latin1 DEFAULT NULL,
  `fim` varchar(7) CHARACTER SET latin1 DEFAULT NULL,
  `contato` varchar(150) CHARACTER SET latin1 DEFAULT NULL COMMENT 'contato do local da PSC',
  `orientador` varchar(150) CHARACTER SET latin1 DEFAULT NULL COMMENT 'Orientador responsável ',
  `atualizado_em` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `criado_em` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `id_socioeducativa_UNIQUE` (`id_anotacao_socioeducativa`),
  UNIQUE KEY `id_sociedade_tipo_medida_UNIQUE` (`id_sociedade_tipo_medida`),
  CONSTRAINT `fk_socioeducativa_familia_membros_1` FOREIGN KEY (`id_sociedade_tipo_medida`) REFERENCES `socioeducativa_tipo_medida` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_socioeducativa_tipo_medida_1` FOREIGN KEY (`id_anotacao_socioeducativa`) REFERENCES `anotacao_socioeducativa` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `anotacao_socioeducativa_familia_membros`
--

LOCK TABLES `anotacao_socioeducativa_familia_membros` WRITE;
/*!40000 ALTER TABLE `anotacao_socioeducativa_familia_membros` DISABLE KEYS */;
/*!40000 ALTER TABLE `anotacao_socioeducativa_familia_membros` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `anotacao_trabalho_renda`
--

DROP TABLE IF EXISTS `anotacao_trabalho_renda`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `anotacao_trabalho_renda` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_atendimento` int(10) unsigned NOT NULL,
  `renda_familiar_sem` varchar(10) CHARACTER SET latin1 DEFAULT NULL COMMENT 'renda familiar SEM programas sociais',
  `renda_per_capta_sem` varchar(10) CHARACTER SET latin1 DEFAULT NULL COMMENT 'renda per capta SEM programas sociais',
  `alguem_familia_aposentado` tinyint(1) DEFAULT '0',
  `renda_familiar_com` varchar(10) CHARACTER SET latin1 DEFAULT NULL COMMENT 'renda familiar COM programas sociais',
  `renda_per_capta_com` varchar(10) CHARACTER SET latin1 DEFAULT NULL COMMENT 'renda per capta COM programas sociais',
  `observacoes` text CHARACTER SET latin1,
  `atualizado_em` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `criado_em` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `id_atendimento_UNIQUE` (`id_atendimento`),
  CONSTRAINT `fk_trabalho_renda_1` FOREIGN KEY (`id_atendimento`) REFERENCES `atendimento` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `anotacao_trabalho_renda`
--

LOCK TABLES `anotacao_trabalho_renda` WRITE;
/*!40000 ALTER TABLE `anotacao_trabalho_renda` DISABLE KEYS */;
/*!40000 ALTER TABLE `anotacao_trabalho_renda` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `anotacao_trabalho_renda_familia`
--

DROP TABLE IF EXISTS `anotacao_trabalho_renda_familia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `anotacao_trabalho_renda_familia` (
  `id` int(10) unsigned NOT NULL,
  `id_anotacao_trabalho_renda` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_membro` varchar(45) DEFAULT NULL,
  `idade` varchar(2) DEFAULT NULL,
  `tem_ctps` tinyint(1) DEFAULT NULL,
  `ocupacao` varchar(255) DEFAULT NULL,
  `possui_qualificacao` tinyint(1) DEFAULT NULL,
  `qualificacao` varchar(255) DEFAULT NULL COMMENT 'identificar a qualificação, caso possua',
  `renda_mensal` varchar(45) DEFAULT NULL,
  `BPC` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `id_trabalho_renda_UNIQUE` (`id_anotacao_trabalho_renda`),
  CONSTRAINT `fk_trabalho_renda_familia_1` FOREIGN KEY (`id_anotacao_trabalho_renda`) REFERENCES `anotacao_trabalho_renda` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `anotacao_trabalho_renda_familia`
--

LOCK TABLES `anotacao_trabalho_renda_familia` WRITE;
/*!40000 ALTER TABLE `anotacao_trabalho_renda_familia` DISABLE KEYS */;
/*!40000 ALTER TABLE `anotacao_trabalho_renda_familia` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `anotacao_violencia`
--

DROP TABLE IF EXISTS `anotacao_violencia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `anotacao_violencia` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_atendimento` int(10) unsigned NOT NULL,
  `acompanhado_data_inicio` datetime DEFAULT NULL,
  `acompanhado_data_fim` datetime DEFAULT NULL,
  `id_unidade` int(10) DEFAULT NULL,
  `observacoes` text CHARACTER SET latin1,
  `atualizado_em` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `criado_em` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `id_atendimento_UNIQUE` (`id_atendimento`),
  CONSTRAINT `fk_violencia_1` FOREIGN KEY (`id_atendimento`) REFERENCES `atendimento` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `anotacao_violencia`
--

LOCK TABLES `anotacao_violencia` WRITE;
/*!40000 ALTER TABLE `anotacao_violencia` DISABLE KEYS */;
/*!40000 ALTER TABLE `anotacao_violencia` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `anotacao_violencia_familia_membro`
--

DROP TABLE IF EXISTS `anotacao_violencia_familia_membro`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `anotacao_violencia_familia_membro` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_anotacao_violencia` int(10) unsigned NOT NULL,
  `id_anotacao_composicao_familiar` int(10) unsigned NOT NULL,
  `trabalho_infantil` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `exploração_sexual` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `abuso_sexual` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `violencia_fisica` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `violencia_psicologica` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `negligencia` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `trajetoria_rua` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `discriminacao` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `atualizado_em` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `criado_em` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_violencia_UNIQUE` (`id`),
  UNIQUE KEY `id_anotacao_violencia_UNIQUE` (`id_anotacao_violencia`),
  UNIQUE KEY `id_composicao_familiar_UNIQUE` (`id_anotacao_composicao_familiar`),
  KEY `fk_violencia_familia_1_idx` (`id_anotacao_violencia`),
  CONSTRAINT `fk_anotacao_violencia_familia_membro_composicao_familiar` FOREIGN KEY (`id_anotacao_composicao_familiar`) REFERENCES `anotacao_composicao_familiar` (`id`),
  CONSTRAINT `fk_violencia_familia_violencia` FOREIGN KEY (`id_anotacao_violencia`) REFERENCES `anotacao_violencia` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `anotacao_violencia_familia_membro`
--

LOCK TABLES `anotacao_violencia_familia_membro` WRITE;
/*!40000 ALTER TABLE `anotacao_violencia_familia_membro` DISABLE KEYS */;
/*!40000 ALTER TABLE `anotacao_violencia_familia_membro` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `atendimento`
--

DROP TABLE IF EXISTS `atendimento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `atendimento` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_pessoa` int(10) unsigned NOT NULL,
  `id_tecnico` int(10) unsigned NOT NULL,
  `id_forma_acesso` int(10) unsigned NOT NULL,
  `id_unidade` int(10) unsigned NOT NULL,
  `nis` varchar(11) DEFAULT NULL,
  `data_hora` datetime DEFAULT NULL COMMENT 'data e hora do atendimento registrada pelo servidor de aplicação',
  `cpf` varchar(14) DEFAULT NULL,
  `pessoa_referencia` varchar(45) DEFAULT NULL,
  `telefone` varchar(45) DEFAULT NULL,
  `sexo` varchar(45) DEFAULT NULL,
  `cor_raca` varchar(45) DEFAULT NULL,
  `nascimento` varchar(45) DEFAULT NULL,
  `idade` varchar(45) DEFAULT NULL,
  `endereco_abrigo` varchar(45) DEFAULT NULL,
  `cep` varchar(45) DEFAULT NULL,
  `endereco` varchar(45) DEFAULT NULL,
  `numero` varchar(45) DEFAULT NULL,
  `complemento` varchar(45) DEFAULT NULL,
  `bairro` varchar(45) DEFAULT NULL,
  `municipio` varchar(45) DEFAULT NULL,
  `uf` varchar(45) DEFAULT NULL,
  `ponto_referencia` varchar(45) DEFAULT NULL,
  `breve_relato` varchar(45) DEFAULT NULL,
  `como_acessou` varchar(45) DEFAULT NULL,
  `detalhe_outros` varchar(45) DEFAULT NULL,
  `nome_contato_orgao` varchar(45) DEFAULT NULL,
  `motivacao_primeiro_atendimento` varchar(45) DEFAULT NULL,
  `bolsa_familia` varchar(45) DEFAULT NULL,
  `bpc` varchar(45) DEFAULT NULL,
  `peti` varchar(45) DEFAULT NULL,
  `outros` varchar(45) DEFAULT NULL,
  `atualizado_em` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `criado_em` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `pessoa_id_UNIQUE` (`id_pessoa`),
  UNIQUE KEY `id_tecnico_UNIQUE` (`id_tecnico`),
  UNIQUE KEY `id_forma_acesso_UNIQUE` (`id_forma_acesso`),
  UNIQUE KEY `id_unidade_UNIQUE` (`id_unidade`),
  KEY `fk_atendimento_pessoa1_idx` (`id_pessoa`),
  CONSTRAINT `fk_atendimento_forma_acesso` FOREIGN KEY (`id_forma_acesso`) REFERENCES `forma_acesso` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_atendimento_pessoa` FOREIGN KEY (`id_pessoa`) REFERENCES `anotacao_pessoa_referencia` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_atendimento_tecnico` FOREIGN KEY (`id_tecnico`) REFERENCES `tecnico` (`id`),
  CONSTRAINT `fk_atendimento_unidade` FOREIGN KEY (`id_unidade`) REFERENCES `unidade` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `atendimento`
--

LOCK TABLES `atendimento` WRITE;
/*!40000 ALTER TABLE `atendimento` DISABLE KEYS */;
/*!40000 ALTER TABLE `atendimento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `atendimento_demanda_apresentada`
--

DROP TABLE IF EXISTS `atendimento_demanda_apresentada`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `atendimento_demanda_apresentada` (
  `id_atendimento` int(10) unsigned NOT NULL,
  `id_demanda` int(10) unsigned NOT NULL,
  `orientacao` tinyint(1) DEFAULT '0',
  `outro_cras` tinyint(1) DEFAULT '0',
  `outro_orgao` tinyint(1) DEFAULT '0',
  UNIQUE KEY `id_atendimento_UNIQUE` (`id_atendimento`),
  UNIQUE KEY `id_demanda_UNIQUE` (`id_demanda`),
  CONSTRAINT `fk_atendimento_demanda_apresentada_1` FOREIGN KEY (`id_atendimento`) REFERENCES `atendimento` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_atendimento_demanda_apresentada_2` FOREIGN KEY (`id_demanda`) REFERENCES `demanda_apresentada` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='registro atendimento - demanda apresentada';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `atendimento_demanda_apresentada`
--

LOCK TABLES `atendimento_demanda_apresentada` WRITE;
/*!40000 ALTER TABLE `atendimento_demanda_apresentada` DISABLE KEYS */;
/*!40000 ALTER TABLE `atendimento_demanda_apresentada` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `demanda_apresentada`
--

DROP TABLE IF EXISTS `demanda_apresentada`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `demanda_apresentada` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(150) CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Registro de atendimento';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `demanda_apresentada`
--

LOCK TABLES `demanda_apresentada` WRITE;
/*!40000 ALTER TABLE `demanda_apresentada` DISABLE KEYS */;
/*!40000 ALTER TABLE `demanda_apresentada` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `forma_acesso`
--

DROP TABLE IF EXISTS `forma_acesso`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `forma_acesso` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `forma_acesso`
--

LOCK TABLES `forma_acesso` WRITE;
/*!40000 ALTER TABLE `forma_acesso` DISABLE KEYS */;
INSERT INTO `forma_acesso` VALUES (4,'BUSCA ATIVA');
/*!40000 ALTER TABLE `forma_acesso` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `socioeducativa_tipo_medida`
--

DROP TABLE IF EXISTS `socioeducativa_tipo_medida`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `socioeducativa_tipo_medida` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `socioeducativa_tipo_medida`
--

LOCK TABLES `socioeducativa_tipo_medida` WRITE;
/*!40000 ALTER TABLE `socioeducativa_tipo_medida` DISABLE KEYS */;
/*!40000 ALTER TABLE `socioeducativa_tipo_medida` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tecnico`
--

DROP TABLE IF EXISTS `tecnico`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tecnico` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_unidade` int(10) unsigned NOT NULL,
  `nome` varchar(255) CHARACTER SET latin1 NOT NULL,
  `atualizado_em` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `criado_em` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `id_unidade_UNIQUE` (`id_unidade`),
  CONSTRAINT `fk_tecnicos_1` FOREIGN KEY (`id_unidade`) REFERENCES `unidade` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tecnico`
--

LOCK TABLES `tecnico` WRITE;
/*!40000 ALTER TABLE `tecnico` DISABLE KEYS */;
INSERT INTO `tecnico` VALUES (6,12,'FÃTIMA','2018-10-18 12:09:33','2018-10-09 13:20:47'),(8,14,'MARIA','2018-10-17 15:00:13','2018-10-17 15:00:13');
/*!40000 ALTER TABLE `tecnico` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipo_beneficio`
--

DROP TABLE IF EXISTS `tipo_beneficio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipo_beneficio` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipo_beneficio`
--

LOCK TABLES `tipo_beneficio` WRITE;
/*!40000 ALTER TABLE `tipo_beneficio` DISABLE KEYS */;
/*!40000 ALTER TABLE `tipo_beneficio` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `unidade`
--

DROP TABLE IF EXISTS `unidade`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `unidade` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) CHARACTER SET latin1 NOT NULL,
  `atualizado_em` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `criado_em` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `unidade`
--

LOCK TABLES `unidade` WRITE;
/*!40000 ALTER TABLE `unidade` DISABLE KEYS */;
INSERT INTO `unidade` VALUES (12,' CRAS II','2018-10-09 16:26:00','2018-10-09 13:19:49'),(14,'CRAS I','2018-10-17 14:59:39','2018-10-17 14:59:39');
/*!40000 ALTER TABLE `unidade` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuario`
--

DROP TABLE IF EXISTS `usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuario` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(50) CHARACTER SET latin1 NOT NULL,
  `email` varchar(60) CHARACTER SET latin1 NOT NULL,
  `senha` varchar(60) CHARACTER SET latin1 NOT NULL,
  `atualizado_em` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `criado_em` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `nome_UNIQUE` (`nome`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuario`
--

LOCK TABLES `usuario` WRITE;
/*!40000 ALTER TABLE `usuario` DISABLE KEYS */;
INSERT INTO `usuario` VALUES (3,'ronaldo.cunha','ronaldo.cunha@salvador.ba.gov.br','$2y$10$C5Drg7p8E2dw.3a4QTaGdelW.N4sWPsfSJyqcUViTwK9w6PbMZRTu','2018-10-10 13:55:51','2018-10-10 13:55:51');
/*!40000 ALTER TABLE `usuario` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-10-23 14:08:22
