<?php

namespace App\Controllers;

use \App\Models\Login;
use \App\core\{
	Utils,
	Controller as Cc
};

class LoginController extends Cc{
	
	private $model;
	public static $utils;

	public static function indexLogin():bool{
		$_SESSION = null;
		session_unset();
		session_destroy();
		return true;
	}

	public static function erroLogin():bool{
		$_SESSION = null;
		session_unset();
		session_destroy();
		return true;
	}

	public static function sairLogin(){
		//session_regenerate_id(false);
		$_SESSION = null;
		session_unset();
		session_destroy();
		Cc::urlRedirect();
	}

	public static function acessarLogin(array $data){
		if(is_array($data)){
			if( isset($data['nome']) AND isset($data['senha']) ){
				//if( Utils::validaEmail($data['email']) ){
					$ret = Login::acessar($data['nome']);
					if($ret){
						if(password_verify($data['senha'], $ret['senha'])){
							$_SESSION['usuario_logado'] = null;
							$_SESSION['expira_sessao']  = null;

							$_SESSION['usuario_logado'] = $ret['id'];
							$_SESSION['expira_sessao']  = time();
							Cc::urlRedirect('home/indexHome');
						}
					}
				//}				
			}
		}
		return false;
	}
}

