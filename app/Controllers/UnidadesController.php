<?php

namespace App\Controllers;
use \App\Models\Unidade;
use \App\core\Controller as Cc;

class UnidadesController extends Cc{

	public function indexUnidades(array $dados){
		return Unidade::listar();
	}

	public function novoUnidades(array $dados){
		return true;
	}

	public function editarUnidades(array $dados){
		if(
			(!isset($dados['id']) OR $dados['id'] == '') 
		){
			Cc::urlRedirect('unidades/novoUnidades?e=1');
			die();
		}
		return Unidade::listaUm($dados['id']);
	}

	public function atualizarUnidades(array $dados){
		if(
			(!isset($dados['id']) OR $dados['id'] == '') AND 
			(!isset($dados['nome']) OR $dados['nome'] == '') 
		){
			Cc::urlRedirect('unidades/indexUnidades');
			die();
		}

		$atu = Unidade::atualizar($dados['id'], $dados['nome']);
		if($atu){
			Cc::urlRedirect('unidades/indexUnidades');
		}else{
			Cc::urlRedirect('unidades/novoUnidades?e=2');
		}
	}

	public function cadastrarUnidades(array $dados){

		if(!isset($dados['nome']) OR $dados['nome'] == '' ){
			Cc::urlRedirect('unidades/novoUnidades?e=1');
			die();
		}
		$cad = Unidade::cadastrar($dados['nome']);
		if($cad){
			Cc::urlRedirect('unidades/indexUnidades');
		}else{
			Cc::urlRedirect('unidades/novoUnidades?e=2');
		}
	}

	public function deletarUnidades(array $dados){
		if(
			(!isset($dados['id']) OR $dados['id'] == '') 
		){
			Cc::urlRedirect('unidades/indexUnidades');
			die();
		}

		$del = Unidade::deletar($dados['id']);
		if($del){
			Cc::urlRedirect('unidades/indexUnidades');
		}else{
			Cc::urlRedirect('unidades/indexUnidades?e=3');
		}
	}
}