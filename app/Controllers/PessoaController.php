<?php

namespace App\Controllers;
use \App\Models\Pessoa;
use \App\core\Controller as Cc;

class PessoaController extends Cc{

	public function indexPessoa(){
		return Pessoa::listarFormaPrimeiroAcesso();
	}

	public function async_listaPessoaAtendimento(array $dados){
		$ret = Pessoa::listaPessoaAtendimento($dados);
		return $ret; exit;
	}

}