<?php

namespace App\Controllers;

use \App\Models\Index;
use \App\core\{
	Utils,
	Controller as CoreController
};

class minhaClasse { use \App\core\Outros; }

class Bar {}

trait SettersTrait{
    /**
     * @param $name
     * @param $value
     */
    public function __set($name, $value){
        $setter = 'set'.$name;
        if (method_exists($this, $setter)) {
            $this->$setter($value);
        } else {
            $this->$name = $value;
        }
    }
}

class IndexController extends CoreController{
	
	use SettersTrait;

	private $model;
	public static $utils;
	private $bar;

	public function __construct(){
		/*if(!parent::__construct()){
			//return false;
			//exit('falhou');
		}

		//self::$utils = new Utils;
		
		print_r( outro());

		echo "<br/>";

		$t = new class{ use \App\Core\Outros; };
		$u = new minhaClasse;

		$u->slottedDriver();
		echo $u->type;
		$t->phillipsDriver();
		echo $t->type;

		exit;
		*/
	}
	
   	protected function setBar(Bar $bar){
        //(optional) Protected so it wont be called directly by external 'entities'
        $this->bar = $bar;
    }

	public static function indexHome(){
		$model = new Index;
		return $model->indexModel();
	}

	public static function listarUm(int $numero):int{
		return $numero;
	}

	public static function listarTodos(int $numero=10):int{
		return $numero*5;
	}

	public static function insereDois(array $dados):array{
		return $dados;
	}

	public static function insereUm(array $dados):bool{
		return true;
	}
}

