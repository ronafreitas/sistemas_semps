<?php

namespace App\Controllers;
use \App\Models\Tecnico;
use \App\core\Controller as Cc;

class TecnicosController{

	public function indexTecnicos(){
		return Tecnico::listar();
	}

	public function novoTecnicos(){
		return Tecnico::listarUnidades();
	}

	public function cadastrarTecnicos(array $dados){
		if(
			(!isset($dados['nome']) OR $dados['nome'] == '') AND 
			(!isset($dados['id_unidade']) OR $dados['id_unidade'] == '')
		){
			Cc::urlRedirect('tecnicos/indexTecnicos?e=1');
			die();
		}

		$cad = Tecnico::cadastrar($dados['nome'], $dados['id_unidade']);
		if($cad){
			Cc::urlRedirect('tecnicos/indexTecnicos');
		}else{
			Cc::urlRedirect('tecnicos/indexTecnicos?e=2');
		}
	}

	public function deletarTecnicos(array $dados){
		if(
			(!isset($dados['id']) OR $dados['id'] == '') 
		){
			Cc::urlRedirect('tecnicos/indexTecnicos');
			die();
		}

		$del = Tecnico::deletar($dados['id']);
		if($del){
			Cc::urlRedirect('tecnicos/indexTecnicos');
		}else{
			Cc::urlRedirect('tecnicos/indexTecnicos?e=2');
		}
	}

	public function editarTecnicos(array $dados){
		if(
			(!isset($dados['id']) OR $dados['id'] == '') 
		){
			Cc::urlRedirect('tecnicos/indexTecnicos?e=1');
			die();
		}
		$undid = Tecnico::listarUnidades();
		$umtec = Tecnico::listaUm($dados['id']);
		return array('unidades'=>$undid,'tecnico'=> $umtec);
	}

	public function atualizarTecnicos(array $dados){
		if(
			(!isset($dados['id']) OR $dados['id'] == '') AND
			(!isset($dados['nome']) OR $dados['nome'] == '') AND
			(!isset($dados['id_unidade']) OR $dados['id_unidade'] == '') 
		){
			Cc::urlRedirect('tecnicos/indexTecnicos?e=1');
			die();
		}
		$atu = Tecnico::atualizar($dados['id'],$dados['nome'],$dados['id_unidade']);
		if($atu){
			Cc::urlRedirect('tecnicos/indexTecnicos');
		}else{
			Cc::urlRedirect('tecnicos/indexTecnicos?e=2');
		}
	}
	
}