<?php

namespace App\Controllers;

use \App\core\ViewDbExterno;

class AcessoExternoController extends ViewDbExterno{

	public function __construct(){
		if(!parent::__construct()){
			//exit('aaaa');
		}
	}

	public function view(array $dados){
		return ViewDbExterno::getDadaos($dados);
	}
}