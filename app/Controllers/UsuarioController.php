<?php

namespace App\Controllers;

use \App\Models\Usuario;
use \App\core\{
	Utils,
	Controller as Cc
};


class UsuarioController extends Cc{
	
	//private $model;
	//public static $utils;

	public function __construct(){
		/*if(!parent::__construct()){
			//return false;
			//exit('falhou');
		}

		//self::$utils = new Utils;
		
		print_r( outro());

		echo "<br/>";

		$t = new class{ use \App\core\Outros; };
		$u = new minhaClasse;

		$u->slottedDriver();
		echo $u->type;
		$t->phillipsDriver();
		echo $t->type;

		exit;
		*/
	}
	
	public function indexUsuario(array $dados):array{
		return Usuario::listar();
	}

	public function umUsuario(array $dados):array{
		return Usuario::listaUm($dados['id'] ?? false);
	}

	public function atualizaUsuario(array $dados){

		$id = (int) $dados['id'];

		if(
			(!isset($dados['id']) OR $dados['id'] == '') AND 
			(!isset($dados['email']) OR $dados['email'] == '') AND 
			(!isset($dados['senha']) OR $dados['senha'] == '') 
		){
			Cc::urlRedirect('usuario/indexUsuario?e=1');
		}

		if( !Utils::validaInteiro($id) ){
			Cc::urlRedirect('usuario/indexUsuario?e=2');
		}

		if( Utils::validaEmail($dados['email']) ){
			$atu = Usuario::atualizar($id, $dados['email'], password_hash($dados['senha'],PASSWORD_DEFAULT));
			if($atu){
				Cc::urlRedirect('usuario/indexUsuario?s=1');
			}else{
				Cc::urlRedirect('usuario/indexUsuario?e=3');
			}
		}else{
			Cc::urlRedirect('usuario/indexUsuario?e=4');
		}
	}

	public function novoUsuario(){
		return ['email'=>''];
	}

	public function cadastrarUsuario(array $dados){
		if(
			(!isset($dados['email']) OR $dados['email'] == '') AND 
			(!isset($dados['senha']) OR $dados['senha'] == '') 
		){
			Cc::urlRedirect('usuario/indexUsuario?e=1');
		}

		if( Utils::validaEmail($dados['email']) ){
			$cad = Usuario::cadastrar($dados['email'], password_hash($dados['senha'],PASSWORD_DEFAULT));
			if($cad){
				Cc::urlRedirect('usuario/indexUsuario?s=1');
			}else{
				Cc::urlRedirect('usuario/indexUsuario?e=2');
			}
		}else{
			Cc::urlRedirect('usuario/indexUsuario?e=3');
		}
	}

	public function deletarUsuario(array $dados){
		$id = (int) $dados['id'];
		if($id){
			$del = Usuario::deletar($id);
			if($del){
				Cc::urlRedirect('usuario/indexUsuario?s=1');
			}else{
				Cc::urlRedirect('usuario/indexUsuario?e=2');
			}
		}else{
			Cc::urlRedirect('usuario/indexUsuario?s=1');
		}
	}
}

