<?php

namespace App\Controllers;
use \App\Models\{
	Atendimento,
	Pessoa,
	Unidade,
	Tecnico
};
use \App\core\Controller as Cc;

class AtendimentoController{

	public function indexAtendimento(array $dados){

		//print_r($dados);	exit('');
		//print_r($_SESSION['usuario_logado']); exit;

		// Cc::urlRedirect('tecnicos/indexTecnicos?e=1');

		if(
			isset($dados['id_unidade']) AND
			isset($dados['atendimento']) AND
			isset($dados['pessoa']) 
		){
			if($dados['atendimento'] == 2){
				$retPe = Pessoa::atendimentoListaPessoa($dados);
				if($retPe){
					$pessoa=$retPe;
				}else{
					$pessoa = [];
				}
			}else{
				$pessoa = null;
			}
		}else{
			$pessoa = null;
		}

		$periodo_ini = $dados['periodo_ini'] ?? '';
		$periodo_fim = $dados['periodo_fim'] ?? '';

		$inputs=array(
			'id_unidade'=>$dados['id_unidade'] ?? null,
			'atendimento'=>$dados['atendimento'] ?? 1,
			'pessoa'=>$dados['pessoa'] ?? null,
			'apelido'=>$dados['apelido'] ?? null,
			'nis'=>$dados['nis'] ?? null,
			'tecnico'=>$dados['tecnico'] ?? null,
			'periodo_ini'=>urldecode($periodo_ini) ?? null,
			'periodo_fim'=>urldecode($periodo_fim) ?? null,
		);
		$_SESSION['pessooa'] = $pessoa;
		return ['unidades'=>Atendimento::listaUnidades(), '_get'=>$inputs, 'pessoa'=>$pessoa];
	}

	public function novoAtendimento(array $dados){
		
		//print_r($dados); exit;

		//$codificada = base64_encode($string);

		//print_r($_SESSION['pessooa'][0]); exit;
		return [
			'formaAcesso'		=>	Atendimento::listaFormaAcesso(),
			'unidades'			=>	Unidade::listar(),
			'tecnicos'			=>	Tecnico::listarTecnicos(),
			//'umaPessoa'			=>	Pessoa::listaUmaPessoa( base64_decode($dados['id']) ),
			'umaPessoa'			=>	Pessoa::listaUmaPessoa( $dados['id'] ),
			'dmdsAprds'			=>	Atendimento::listaDemandasApresentadas(),
			'listPrimAtendt'	=>	Atendimento::listaIngressoPrimeiroAtendimento()
		];
	}
	
	public function editarAtendimento(){
		return true;
	}

	public function acompanhamentoAtendimento(){
		return true;
	}

	public function cadastrarTecnicos(array $dados){
		if(
			(!isset($dados['nome']) OR $dados['nome'] == '') AND 
			(!isset($dados['id_unidade']) OR $dados['id_unidade'] == '')
		){
			Cc::urlRedirect('tecnicos/indexTecnicos?e=1');
			die();
		}

		$cad = Tecnico::cadastrar($dados['nome'], $dados['id_unidade']);
		if($cad){
			Cc::urlRedirect('tecnicos/indexTecnicos');
		}else{
			Cc::urlRedirect('tecnicos/indexTecnicos?e=2');
		}
	}

	public function cadastrarAtendimento(array $dados){
		
		//echo "<pre>"; print_r($dados); exit;

		$cad = Atendimento::cadastrar($dados);
		if($cad){
			Cc::urlRedirect('atendimento/acompanhamentoAtendimento?id_do_atendimento=155');
		}else{
			Cc::urlRedirect('atendimento/novoAtendimento?id=3&e=1');
		}
/*
    [id_pessoa] => 3
    [data] => 25/10/2018
    [hora] => 14:40
    [id_forma_acesso] => 4
    [id_unidade] => 14
    [id_tecnico] => 8
    [nis] => 111.1111.111-1
    [cpf] => 555.555.555-5
    [nome_pessoa] => nome pesso
    [telefone] => (88)8888
    [sexo] => 2
    [cor_raca] => 2
    [data_nascimento] => 17/10/2018
    [endereco_abrigo] => on
    [cep_endereco] => 40301-210
    [rua_endereco] => Rua Monte Castelo
    [numero_endereco] => 55
    [complemento_endereco] => comple
    [bairro_endereco] => Barbalho
    [municipio_endereco] => Salvador
    [estado_endereco] => BA
    [referencia_endereco] => ponto ref
    [demanda_apresentada] => Array
        (
            [0] => Array
                (
                    [id_demanda_apresentada] => 1
                    [orientacao] => on
                    [outro_cras] => on
                )

        )

    [breve_atendimento] => breve aten
    [id_primeiro_atendimento] => 1
    [nome_orgao] => nome se contato
    [motivacao_atendimento] => motivo 1 aten
    [bolsa_familia] => on
    [bpc] => on
    [peti] => on
    [outros] => on


*/

	}

}