<?php

namespace App\Core;

trait Outros{
	
	public $type;
    
    public function slottedDriver() {
        
        $this->type = 'slotted';
        
    }
    
    public function phillipsDriver() {
        
        $this->type = 'phillips';
        
    }
    
    public function starDriver() {
        
        $this->type = 'star';
        
    }
    
}