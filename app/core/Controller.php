<?php

namespace App\Core;

class Controller{
	
	private static $url_redirect = 'http://localhost/prontuario/';
	//private static $url_redirect = 'http://prontuariosocial.homologa.salvador.ba.gov.br/';

	public static function urlRedirect(string $url=''){
		header('Location: '.self::$url_redirect.$url);
		die();
	}
}