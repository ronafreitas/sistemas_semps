<?php

namespace App\Core;

class Utils{
	
	public static function validaEmail(string $email):bool{
		return filter_var($email, FILTER_VALIDATE_EMAIL);
	}

	public static function validaInteiro(int $int):bool{
		return filter_var($int, FILTER_VALIDATE_INT);
	}

	public static function parseDate($date, $outputFormat = 'd/m/Y'){
	    $formats = array(
	        'd/m/Y',
	        'd/m/Y H',
	        'd/m/Y H:i',
	        'd/m/Y H:i:s',
	        'Y-m-d',
	        'Y-m-d H',
	        'Y-m-d H:i',
	        'Y-m-d H:i:s',
	    );

	    foreach($formats as $format){
	        $dateObj = DateTime::createFromFormat($format, $date);
	        if($dateObj !== false){
	            break;
	        }
	    }

	    if($dateObj === false){
	        throw new Exception('Invalid date:' . $date);
	    }

	    return $dateObj->format($outputFormat);
	}

	public static function stringToRadio(string $string){
		$opcpes = explode(",", $string);
		foreach ($opcpes as $value) {
			//<input type="radio" name="gender" value="male"> Male<br>
		}
	}

	protected function moeda($get_valor) {
		$source = array('.', ','); 
		$replace = array('', '.');
		$valor = str_replace($source, $replace, $get_valor); //remove os pontos e substitui a virgula pelo ponto
		return $valor; //retorna o valor formatado para gravar no banco
	}

    /* TRansforma data padro brasil para padro DATE MYSQL YYYY-MM-DD
     * @autor Ronaldo
     * return string
    */
	public function dataToMysql($data){
		$dts = explode('/',$data);
		return $dts[2].'-'. $dts[1].'-'. $dts[0];
                //$date = DateTime::createFromFormat('d/m/Y H:i', $data); return $date->format('Y-m-d');
	}
    
    /*
     * @autor Ronaldo
     * return string
    */
	public function dataBrasil($data){
		// apenas PHP 5.3 >
		//$date = DateTime::createFromFormat('Y-m-d', $data); 
		//return $date->format('d/m/Y');
		return date('d/m/Y', strtotime($data));
	}

	public function moedaBrasil($valor,$tipo=1){
		if($tipo ==2)	return  number_format($valor, 2, ',', '.');
		else return  number_format($valor, 2, ',', '.');
	}


    /* tipo um bool para identificar qual  o selected de um combo
     * @autor Ronaldo
     * return string
    */
	public function selected( $value, $selected ){
		return $value==$selected ? ' selected="selected"' : '';
	}
        
	public function checked( $value, $selected ){
		return $value==$selected ? ' checked' : '';
	}
	public function disabled( $value, $selected ){
		return $value==$selected ? ' ' : 'disabled="disabled"';
	}

	public static function geraTimestamp($data) {
		$partes = explode('/', $data);
		return mktime(0, 0, 0, $partes[1], $partes[0], $partes[2]);
	}

    /* conta intervalo entre datas
     * @autor Ronaldo
     * return string
    */
	public function interdates($inicio,$fim){
            //date_default_timezone_set('America/Sao_Paulo');

            $time_inicial = self::geraTimestamp($inicio);
            $time_final = self::geraTimestamp($fim);
            $diferenca = $time_final - $time_inicial; 
            $dias = (int)floor( $diferenca / (60 * 60 * 24)); 

            /*
            $inicio = DateTime::createFromFormat('d/m/Y', $inicio);
            $fim = DateTime::createFromFormat('d/m/Y', $fim);
            $intervalo = $inicio->diff($fim);
            return $intervalo->days;*/
            return $dias;
	}

    /* completa um determinado com zeros
     * @autor Ronaldo
     * return int
    */
    public function zerofill ($num, $zerofill) {
        return str_pad($num, $zerofill, '0', STR_PAD_RIGHT);
        //return $num;
    }
    
    private function folder_exist($folder) {
        $path = realpath($folder);

        if($path !== false AND is_dir($path)) {
            return $path;
        }
        return false;
    }
    
    public function criarPasta($chave=null){
        if(!self::folder_exist('../uploads/'.$chave)){
           //chmod ("/arquivo/diretorio", 0755);
           mkdir('../uploads/'.$chave, 0777, true);
           return '../uploads/'.$chave;
        }else{
            return false;
        }
    }

}