<?php

namespace App\Core;

class Rotas{

	private $rotas;
	private static $rootUrl='http://localhost/prontuario/';
	//private static $rootUrl='http://prontuariosocial.homologa.salvador.ba.gov.br/';

	public function __construct(){
		$array_rotas = [];

		$array_rotas['login'] = [
			['action'=>'indexLogin','controller'=>'LoginController'],
			['action'=>'sairLogin','controller'=>'LoginController'],
			['action'=>'erroLogin','controller'=>'LoginController'],
			['action'=>'acessarLogin','controller'=>'LoginController'],
		];

		$array_rotas['home'] = [
			['action'=>'indexHome','controller'=>'IndexController']
		];

		$array_rotas['atendimento']	= [
			['action'=>'indexAtendimento','controller'=>'AtendimentoController'],
			['action'=>'novoAtendimento','controller'=>'AtendimentoController'],
			['action'=>'editarAtendimento','controller'=>'AtendimentoController'],
			['action'=>'acompanhamentoAtendimento','controller'=>'AtendimentoController'],
			['action'=>'cadastrarAtendimento','controller'=>'AtendimentoController'],
		];

		$array_rotas['composicao-familiar'] = [
			['action'=>'indexComposicao','controller'=>'ComposicaoFamiliarController'],
		];

		$array_rotas['habitacionais'] = [
			['action'=>'indexCondicoes','controller'=>'CondicoesHabitacionaisController'],
			['action'=>'novoCondicoes','controller'=>'CondicoesHabitacionaisController'],
			['action'=>'editarCondicoes','controller'=>'CondicoesHabitacionaisController'],
		];

		$array_rotas['educacionais'] = [
			['action'=>'indexCondicoesEducacionais','controller'=>'CondicoesEducacionaisController'],
			['action'=>'novoCondicoesEducacionais','controller'=>'CondicoesEducacionaisController'],
			['action'=>'editarCondicoesEducacionais','controller'=>'CondicoesEducacionaisController'],
		];

		$array_rotas['trabalhorenda'] = [
			['action'=>'indexTrabalhoRenda','controller'=>'TrabalhoRendaController'],
			['action'=>'novoTrabalhoRenda','controller'=>'TrabalhoRendaController'],
		];

		$array_rotas['saude'] = [
			['action'=>'indexSaude','controller'=>'CondicoesSaudeController'],
			['action'=>'novoSaude','controller'=>'CondicoesSaudeController'],
			['action'=>'editarSaude','controller'=>'CondicoesSaudeController'],
		];

		$array_rotas['beneficios'] = [
			['action'=>'indexBeneficio','controller'=>'BeneficiosEventuaisController'],
			['action'=>'novoBeneficio','controller'=>'BeneficiosEventuaisController'],
			['action'=>'editarBeneficio','controller'=>'BeneficiosEventuaisController'],
		];

		$array_rotas['violencia'] = [
			['action'=>'indexViolencia','controller'=>'ViolenciaController'],
			['action'=>'novoViolencia','controller'=>'ViolenciaController'],
			['action'=>'editarViolencia','controller'=>'ViolenciaController'],
		];

		$array_rotas['convivencia'] = [
			['action'=>'indexConvivencia','controller'=>'ConvivenciaController'],
			['action'=>'novoConvivencia','controller'=>'ConvivenciaController'],
			['action'=>'editarConvivencia','controller'=>'ConvivenciaController'],
		];

		$array_rotas['fortalecimento'] = [
			['action'=>'indexFortalecimento','controller'=>'FortalecimentoController'],
			['action'=>'novoFortalecimento','controller'=>'FortalecimentoController'],
			['action'=>'editarFortalecimento','controller'=>'FortalecimentoController'],
		];

		$array_rotas['socioeducativas'] = [
			['action'=>'indexSocioeducativas','controller'=>'SocioeducativasController'],
			['action'=>'novoSocioeducativas','controller'=>'SocioeducativasController'],
			['action'=>'editarSocioeducativas','controller'=>'SocioeducativasController'],
		];

		$array_rotas['acolhimento'] = [
			['action'=>'indexAcolhimento','controller'=>'AcolhimentoController'],
			['action'=>'novoAcolhimento','controller'=>'AcolhimentoController'],
			['action'=>'editarAcolhimento','controller'=>'AcolhimentoController'],
		];

		$array_rotas['acompanhamento'] = [
			['action'=>'indexAcompanhamento','controller'=>'AcompanhamentoController'],
			['action'=>'novoAcompanhamento','controller'=>'AcompanhamentoController'],
			['action'=>'editarAcompanhamento','controller'=>'AcompanhamentoController'],
		];
		
		$array_rotas['tecnicos'] = [
			['action'=>'indexTecnicos','controller'=>'TecnicosController'],
			['action'=>'novoTecnicos','controller'=>'TecnicosController'],
			['action'=>'editarTecnicos','controller'=>'TecnicosController'],
			['action'=>'cadastrarTecnicos','controller'=>'TecnicosController'],
			['action'=>'atualizarTecnicos','controller'=>'TecnicosController'],
			['action'=>'deletarTecnicos','controller'=>'TecnicosController'],
		];
		
		$array_rotas['unidades'] = [
			['action'=>'indexUnidades','controller'=>'UnidadesController'],
			['action'=>'novoUnidades','controller'=>'UnidadesController'],
			['action'=>'editarUnidades','controller'=>'UnidadesController'],
			['action'=>'cadastrarUnidades','controller'=>'UnidadesController'],
			['action'=>'atualizarUnidades','controller'=>'UnidadesController'],
			['action'=>'deletarUnidades','controller'=>'UnidadesController'],
		];

		$array_rotas['pessoa'] = [
			['action'=>'indexPessoa','controller'=>'PessoaController'],
			['action'=>'async_listaPessoaAtendimento','controller'=>'PessoaController'],
		];

		$array_rotas['externo']	= [
			['action'=>'view','controller'=>'AcessoExternoController'],
		];

		$this->rotas = $array_rotas;
	}

	public function getUrl(string $url, string $action, array $data){
		$retn=false;
		if(isset($this->rotas[$url])){
			foreach($this->rotas[$url] as $rota){
				if($rota['action'] == $action){
					$retn = $rota;
				}
			}
		}
		return $retn;
	}

	public function validaSessaoToken(){

		if($_SERVER['REQUEST_METHOD'] === 'POST'){

			if(!isset($_POST['input']['token'])){
				$_SESSION = null;
				session_unset();
				session_destroy();
				self::redirecionarRoot();
			}

			if(isset($_POST['input']['token'])){
				if($_POST['input']['token'] !== $_SESSION['token']){
					$_SESSION = null;
					session_unset();
					session_destroy();
					self::redirecionarRoot();
				}
			}
		}else{
			if(isset($_POST['input']['token'])){
				if($_POST['input']['token'] !== $_SESSION['token']){
					$_SESSION = null;
					session_unset();
					session_destroy();
					self::redirecionarRoot();
				}
			}
		}

		if(isset($_SESSION['expira_sessao'])){
			if($_SESSION['expira_sessao'] < (time() - 3600) ){
				$_SESSION = null;
				session_unset();
				session_destroy();
				self::redirecionarRoot();
			}
		}
	}


	public function redirecionarRoot(){
		header('Location: '.self::$rootUrl);
		exit();
	}

	public function redirecionarHome(){
		header('Location: '.self::$rootUrl.'/home/indexHome');
		exit();
	}

	public function redirecionarErroLogin(){
		header('Location: '.self::$rootUrl.'/login/erroLogin');
		exit();
	}

}