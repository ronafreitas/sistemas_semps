<?php

namespace App\Models;
use \App\core\Db;

class Index{
	
	public static function indexModel(){
		$db  = Db::getDb();
		$sql = 'SELECT * FROM teste';
		$sth = $db->prepare($sql);
		$sth->execute();
		//return $sth->fetch(\PDO::FETCH_OBJ);
		return $sth->fetch(\PDO::FETCH_ASSOC);
		$sth = null;
		$db  = null;
	}

	public static function insereUm(string $texto){
		$db  = Db::getDb();
		$sql = "INSERT INTO `teste` ";
		$sql .= "(`texto`) ";
		$sql .= "VALUES('".$texto."');";
		$sth = $db->prepare($sql);
		return $sth->execute();
		$sth = null;
		$db  = null;
	}
}