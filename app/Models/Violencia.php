<?php

namespace App\Models;
use \App\core\Db;

class Violencia{
	
	public static function listaUnidades(){
		$db  = Db::getDb();
		$sql = 'SELECT * FROM unidade ORDER BY nome ASC;';
		$sth = $db->prepare($sql);
		$sth->execute();
		return $sth->fetchAll();
	}

	public static function listar(){
		$db  = Db::getDb();
		$sql = 'SELECT * FROM unidade ORDER BY id DESC ';
		$sth = $db->prepare($sql);
		$sth->execute();
		return $sth->fetchAll();
	}

	public static function listaUm(int $id){
		$db  = Db::getDb();
		$sql = 'SELECT * FROM unidade WHERE id = '.$id.' LIMIT 1;';
		$sth = $db->prepare($sql);
		$sth->execute();
		return $sth->fetchAll();
	}

	public static function cadastrar(string $nome){
		$db  = Db::getDb();
		$sql = "INSERT INTO unidade ";
		$sql .= "(nome)";
		$sql .= "VALUES ('".$nome."')";
		$sth = $db->prepare($sql);
		return $sth->execute();
	}

	public static function atualizar(int $id,string $nome){
		$db  = Db::getDb();
		$sql = "UPDATE unidade ";
		$sql .= "SET nome='".$nome."' ";
		$sql .= "WHERE id = ".$id.";";
		$sth = $db->prepare($sql);
		return $sth->execute();
	}

	public static function deletar(int $id){
		$db  = Db::getDb();
		$sql = "DELETE FROM unidade WHERE id = ".$id.";";
		$sth = $db->prepare($sql);
		return $sth->execute();
	}

}