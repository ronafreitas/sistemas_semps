<?php

namespace App\Models;
use \App\core\Db;

class Atendimento{
	
	public static function listaUnidades(){
		$db  = Db::getDb();
		$sql = 'SELECT * FROM unidade ORDER BY nome ASC;';
		$sth = $db->prepare($sql);
		$sth->execute();
		return $sth->fetchAll();
	}
	
	public static function listaFormaAcesso(){
		$db  = Db::getDb();
		$sql = 'SELECT * FROM forma_acesso ORDER BY nome ASC;';
		$sth = $db->prepare($sql);
		$sth->execute();
		return $sth->fetchAll();
	}

	public static function listaDemandasApresentadas(){
		$db  = Db::getDb();
		$sql = 'SELECT * FROM demanda_apresentada ORDER BY nome ASC;';
		$sth = $db->prepare($sql);
		$sth->execute();
		return $sth->fetchAll();
	}
	
	public static function listaIngressoPrimeiroAtendimento(){
		$db  = Db::getDb();
		$sql = 'SELECT * FROM anotacao_forma_ingresso_primeiro_atendimento ORDER BY id ASC;';
		$sth = $db->prepare($sql);
		$sth->execute();
		return $sth->fetchAll();
	}

	public static function cadastrar(array $dados){
		echo "teste: <pre>";
		print_r($dados);

/*
INSERT INTO `prontuario`.`atendimento`
(`id_pessoa`,`id_tecnico`,`id_forma_acesso`,`id_unidade`,`nis`,`data_hora`,`cpf`,
`pessoa_referencia`,
`telefone`,
`sexo`,
`cor_raca`,
`nascimento`,
`idade`,
`endereco_abrigo`,
`cep`,
`endereco`,
`numero`,
`complemento`,
`bairro`,
`municipio`,
`uf`,
`ponto_referencia`,
`breve_relato`,
`como_acessou`,
`detalhe_outros`,
`nome_contato_orgao`,
`motivacao_primeiro_atendimento`,
`bolsa_familia`,
`bpc`,
`peti`,
`outros`,
`atualizado_em`,
`criado_em`)
VALUES
(<{id_pessoa: }>,
<{id_tecnico: }>,
<{id_forma_acesso: }>,
<{id_unidade: }>,
<{nis: }>,
<{data_hora: }>,
<{cpf: }>,
<{pessoa_referencia: }>,
<{telefone: }>,
<{sexo: }>,
<{cor_raca: }>,
<{nascimento: }>,
<{idade: }>,
<{endereco_abrigo: }>,
<{cep: }>,
<{endereco: }>,
<{numero: }>,
<{complemento: }>,
<{bairro: }>,
<{municipio: }>,
<{uf: }>,
<{ponto_referencia: }>,
<{breve_relato: }>,
<{como_acessou: }>,
<{detalhe_outros: }>,
<{nome_contato_orgao: }>,
<{motivacao_primeiro_atendimento: }>,
<{bolsa_familia: }>,
<{bpc: }>,
<{peti: }>,
<{outros: }>,
<{atualizado_em: CURRENT_TIMESTAMP}>,
<{criado_em: CURRENT_TIMESTAMP}>);

*/

		/*
		$db  = Db::getDb();
		$sql = 'SELECT * FROM forma_acesso ORDER BY nome ASC;';
		$sth = $db->prepare($sql);
		return $sth->execute();
		*/
	}

}