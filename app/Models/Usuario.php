<?php

namespace App\Models;
use \App\core\Db;

class Usuario{
	
	public static function listar(){
		$db  = Db::getDb();
		$sql = 'SELECT * FROM usuarios ORDER BY id DESC ';
		$sth = $db->prepare($sql);
		$sth->execute();
		//return $sth->fetch(\PDO::FETCH_OBJ);
		//return $sth->fetch(\PDO::FETCH_ASSOC);
		//return $sth->fetch();
		return $sth->fetchAll();
		/*$sth->close();
		$db->close();
		$sth = null;
		$db  = null;*/
	}

	public static function listaUm(int $id){
		$db  = Db::getDb();
		$sql = 'SELECT * FROM usuarios WHERE id = '.$id.' ';
		$sth = $db->prepare($sql);
		$sth->execute();
		return $sth->fetchAll();
	}

	public static function cadastrar(string $email,string $senha){
		$db  = Db::getDb();
		$sql = "INSERT INTO usuarios ";
		$sql .= "(email, senha)";
		$sql .= "VALUES ('".$email."', '".$senha."')";
		$sth = $db->prepare($sql);
		return $sth->execute();
	}

	public static function atualizar(int $id,string $email,string $senha){
		$db  = Db::getDb();
		$sql = "UPDATE usuarios ";
		$sql .= "SET email='".$email."', senha='".$senha."' ";
		$sql .= "WHERE id = ".$id.";";
		$sth = $db->prepare($sql);
		return $sth->execute();
	}

	public static function deletar(int $id){
		$db  = Db::getDb();
		$sql = "DELETE FROM usuarios WHERE id = ".$id.";";
		$sth = $db->prepare($sql);
		return $sth->execute();
	}

}