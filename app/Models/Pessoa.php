<?php

namespace App\Models;
use \App\core\Db;

class Pessoa{

	public static function listarFormaPrimeiroAcesso(){
		$db  = Db::getDb();
		$sql = 'SELECT * FROM anotacao_forma_ingresso_primeiro_atendimento ORDER BY id ASC;';
		$sth = $db->prepare($sql);
		$sth->execute();
		return $sth->fetchAll();
	}

	public static function atendimentoListaPessoa(array $dados){
		//print_r($dados['[id_unidade']); exit;

		$db  = Db::getDb();
		
		$wheresql ='(';
		// ----------------------------------------------------------------
			$wheresql.='a.id_unidade = '.$dados['id_unidade'].' AND ';
			$wheresql.='a.nome LIKE \''.$dados['pessoa'].'%\' ';
		// ----------------------------------------------------------------
		$wheresql.=')';
		$sql = "SELECT a.id,a.nome,a.nis,u.nome AS unidade FROM anotacao_pessoa_referencia AS a ";
		$sql .= " INNER JOIN unidade AS u ON u.id = a.id_unidade ";
		$sql .= " WHERE ".$wheresql;
		//echo $sql; exit;

		$sth = $db->prepare($sql);
		$sth->execute();
		return $sth->fetchAll( \PDO::FETCH_ASSOC );
	}

	public static function listaUmaPessoa(int $id){
		$db  = Db::getDb();
		$sql = "SELECT *, DATE_FORMAT(data_nascimento,'%d/%m/%Y') AS data_nascimento FROM anotacao_pessoa_referencia "; 
		$sql .= 'WHERE id = '.$id.';';
		$sth = $db->prepare($sql);
		$sth->execute();
		return $sth->fetchAll();
	}

}