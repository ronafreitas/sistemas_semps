<?php

namespace App\Models;
use \App\core\Db;

class Tecnico{
	
	public static function listarUnidades(){
		$db  = Db::getDb();
		$sql = 'SELECT * FROM unidade ORDER BY id DESC ';
		$sth = $db->prepare($sql);
		$sth->execute();
		return $sth->fetchAll();
	}

	public static function listarTecnicos(){
		$db  = Db::getDb();
		$sql = 'SELECT * FROM tecnico ORDER BY id DESC;';
		$sth = $db->prepare($sql);
		$sth->execute();
		return $sth->fetchAll();
	}

	public static function listar(){
		$db  = Db::getDb();
		$sql = 'SELECT te.id, te.nome, un.nome AS unidade FROM tecnico AS te ';
		$sql .= 'INNER JOIN unidade AS un ';
		$sql .= 'ON te.id_unidade = un.id ';
		$sql .= 'ORDER BY te.id DESC ';
		$sth = $db->prepare($sql);
		$sth->execute();
		return $sth->fetchAll();
	}

	public static function listaUm(int $id){
		$db  = Db::getDb();
		$sql = 'SELECT te.id,te.id_unidade, te.nome, un.nome AS unidade FROM tecnico AS te ';
		$sql .= 'INNER JOIN unidade AS un ';
		$sql .= 'ON te.id_unidade = un.id ';
		$sql .= 'WHERE te.id = '.$id.';';
		$sth = $db->prepare($sql);
		$sth->execute();
		return $sth->fetchAll();
	}

	public static function cadastrar(string $nome, int $id_unidade){
		$db  = Db::getDb();
		$sql = "INSERT INTO tecnico ";
		$sql .= "(id_unidade,nome) ";
		$sql .= "VALUES (".$id_unidade.",'".$nome."');";
		$sth = $db->prepare($sql);
		return $sth->execute();
	}

	public static function atualizar(int $id,string $nome, int $id_unidade){
		$db  = Db::getDb();
		$sql = "UPDATE tecnico ";
		$sql .= "SET nome='".$nome."', id_unidade=".$id_unidade." ";
		$sql .= "WHERE id = ".$id.";";
		$sth = $db->prepare($sql);
		return $sth->execute();
	}

	public static function deletar(int $id){
		$db  = Db::getDb();
		$sql = "DELETE FROM tecnico WHERE id = ".$id.";";
		$sth = $db->prepare($sql);
		return $sth->execute();
	}

}