<?php
session_name('JSESSIONID');
session_start();

/*
$_SESSION=null;
unset($_SESSION);
session_destroy();
exit;*/


/*


$salt = 'kY>xPuvsVN663pyNGX-4BA8[]{r?bNQEP5^x10aMXRBSAP[[bhr[?(22o62J<(ZN';
$sessioname  = crypt($_SERVER['REMOTE_ADDR'].$_SERVER['HTTP_USER_AGENT'], $salt);
session_name( $sessioname );
$https = isset($secure) ? $secure : isset($_SERVER['HTTPS']);
session_set_cookie_params($limit, $path, $domain, $https, true);
session_cache_expire(10); // 10 minutos
session_start();
$_SESSION['user_logado'] = $sessioname;

- php.ini definir session.cookie_httponly e definir seu valor como 1 e também expose_php = off




- alterar 
 ServerTokens Prod
 ServerSignature Off

no arquivo: /etc/apache2/conf-enabled/security.conf
 



https://geridmte.dataprev.gov.br/cas/login;jsessionid=vEbcx+1+9Lr1UIPIL58b5X3O.slave4:cas-mte?service=https%3A%2F%2Fctps.mte.gov.br%2Fintra%2Fctps%2F%3Bjsessionid%3DmIj6Dk9Hf2IiAXe8IGwZZGLV.4ed75b69-0b46-39a6-8f21-8fd195ebc881

*/

require_once "vendor/autoload.php";
use \App\core\Rotas;

try{

	$r = new Rotas;

	$r->validaSessaoToken();
	
	(string) $url_controller = $_GET['controller'] ?? 'login';  // .htaccess
	(string) $action     	 = $_GET['action'] ?? 'indexLogin'; // .htaccess
	(string) $querystr 		 = parse_url($_SERVER['REQUEST_URI'], PHP_URL_QUERY);
	(array)  $GET 			 = [];
	if($querystr){
		$querystr = explode('&',$querystr);
		foreach($querystr as $value){
			$divide = explode('=',$value);
			$GET[$divide[0]]=$divide[1];
		}
	}
	(array) $input = $_POST['input'] ?? $GET;
	// adicionar verificacao de token na URL 

	$rota		= $r->getUrl($url_controller,$action,$input);
	$classname  = '\App\Controllers\\'.$rota['controller'];
	$instancia  = new $classname;

	$load = new \Twig_Loader_Filesystem(__DIR__.'/app/Views/');
	$twig = new \Twig_Environment($load,[
		'debug'=>true
		//,'cache' => __DIR__.'/app/Views/_cache/'
	]);
	$twig->addGlobal('session', $_SESSION);

	$twig->addFunction(
	    new \Twig_SimpleFunction(
	        'form_token',
	        function($lock_to = null) {
	            if (empty($_SESSION['token'])) {
	                $_SESSION['token'] = bin2hex(random_bytes(32));
	            }
	            if (empty($_SESSION['token2'])) {
	                $_SESSION['token2'] = random_bytes(32);
	            }
	            if (empty($lock_to)) {
	                return $_SESSION['token'];
	            }
	            return hash_hmac('sha256', $lock_to, $_SESSION['token2']);
	        }
	    )
	);

	if(!isset($_SESSION['usuario_logado'])){
		if($url_controller == 'login' AND $action == 'indexLogin'){
			echo $twig->render('login/indexLogin.html');
		}elseif($url_controller == 'login' AND $action == 'acessarLogin'){
			$retorno = $instancia->$action($input);
			if(!$retorno){
				$r->redirecionarErroLogin();
			}
		}elseif($url_controller == 'login' AND $action == 'erroLogin'){
			echo $twig->render('login/erroLogin.html');
		}else{
			$r->redirecionarRoot();
		}
	}else{
		if($url_controller == 'login' AND $action == 'indexLogin'){
			$r->redirecionarHome();
		}else{
			$retorno = $instancia->$action($input);
			echo $twig->render($url_controller.'/'.$rota['action'].'.html', ['retorno' => $retorno]);
		}
	}
}catch(Throwable $t){
	if($t->getMessage()){
		$prod_mod=false;
		if($prod_mod){
			header("HTTP/1.1 403 Forbidden", true, 403);
			http_response_code(403);
			exit('<center><h1>Pagina nao encontrada<h1></center>');
		}else{
			echo "<pre>"; print_r($t); echo "</pre>";
			exit('<center><h1>Pagina nao encontrada<h1></center>');
		}
	}
	exit(false);
}
